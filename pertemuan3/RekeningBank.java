package id.ac.nurulfikri.hilmiat.pertemuan3;

/**
 * Created by my on 8/20/16.
 */
public class RekeningBank {
    // property / variable
    private String nomer_rekening;
    private String nama_pemilik;
    private double saldo;


    public static void getNamaBank(){
        System.out.println("BANK MITRA SEJAHTERA BERSMA ");
    }

    //method konstruktor --> dipanggil ketika instance dibuat
    //namanya sama dengan nama kelas
    public RekeningBank() {
        System.out.println("Membuat rekening baru");
    }

    public RekeningBank(String nomer_rekening, String nama_pemilik, double saldo) {
        this.nomer_rekening = nomer_rekening;
        this.setNama_pemilik(nama_pemilik);
        this.saldo = saldo;
    }

    private void setNama_pemilik(String nama){
        this.nama_pemilik = nama.toUpperCase();
    }
    public String getNama_pemilik(){
        return this.nama_pemilik;
    }

    public String getNomer_rekening() {
        return nomer_rekening;
    }

    public double getSaldo() {
        return saldo;
    }

    // method / fungsi
    boolean setor(double jml_setor){
        saldo = saldo+jml_setor; //saldo+=jml_setor
        return true;
    }
    boolean tarik(double jml_tarik){
        boolean status;
        //cek, jml saldo cukup atau tdk
        if(saldo<jml_tarik){
            status = false;
        }else{
            saldo -= jml_tarik;
            status = true;
        }
        return status;
    }
    void cetak_informasi(){
        System.out.println("Data Rekening Tabungan");
        System.out.println("Nama Pemilik:"+nama_pemilik);
        System.out.println("NO Rekening:"+nomer_rekening);
        System.out.println("Saldo:"+saldo);
    }
    boolean transfer(RekeningBank tujuan,double jml_transfer){
        if(this.tarik(jml_transfer)){
            tujuan.setor(jml_transfer);
            return true;
        }else{
            return false;
        }
    }

    public String toString() {
        return "RekeningBank milik "+this.getNama_pemilik();
    }
}
