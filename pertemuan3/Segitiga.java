package id.ac.nurulfikri.hilmiat.pertemuan3;

/**
 * Created by my on 8/20/16.
 */
public class Segitiga {
    //punya apa?
    private float alas,tinggi,luas,keliling;

    //bisa apa?
    public Segitiga(float alas, float tinggi) {
        this.alas = alas;
        this.tinggi = tinggi;
        this.luas = alas * tinggi/2;
    }

    public float getAlas() {
        return alas;
    }

    public float getTinggi() {
        return tinggi;
    }

    public void setTinggi(float tinggi) {
        this.tinggi = tinggi;
    }

    public float getLuas() {
        return luas;
    }

    public float getKeliling() {
        return keliling;
    }

}
