package id.ac.nurulfikri.hilmiat.pertemuan2;

/**
 * Created by my on 8/20/16.
 */
public class LatihanSoal {
    public static void main(String[] args) {
        System.out.println("draw square");
        drawSquare(4);
        System.out.println("draw square hollow");
        drawSquareHollow(4);
        System.out.println("draw pyramid");
        drawPyramid(4);
        System.out.println("draw pyramid hollow");
        drawPyramidHollow(4);
        System.out.println("draw inverted pyramid");
        drawPyramidInverted(4);
        System.out.println("draw inverted pyramid hollow");
        drawPyramidInvertedHollow(4);

    }
    public static void drawSquare(int size){
        for(int baris=0;baris<size;baris++){
            for(int kolom=0;kolom<size;kolom++){
                System.out.print("* ");
            }
            System.out.println("<-- ini baris ke-"+baris);
        }
    }
    public static void drawSquareHollow(int size){
        for(int baris=0;baris<size;baris++){
            //draw each column
            for(int kolom=0;kolom<size;kolom++){
                //draw only first row,first column,last row,last column
                if(baris==0 || baris==(size-1) || kolom==0 || kolom==(size-1)) {
                    System.out.print("* ");
                }else{
                    //draw space
                    System.out.print("  ");
                }
            }
            //go to next row (new line)
            System.out.print("\n");
        }
    }
    public static void drawPyramid(int size){
        //for each row
        for(int baris=0;baris<size;baris++) {
            //draw space (baris,baris-1,baris-2,...)
            for (int a = 0; a < (size-baris); a++) {
                System.out.print(" ");
            }
            //draw star (1,3,5,..)
            for (int b = 0; b <= (baris*2); b++) {
                System.out.print("*");
            }
            //go to next row (new line)
            System.out.print("\n");
        }
    }
    public static void drawPyramidHollow(int size){
        //for each row
        for(int baris=0;baris<size;baris++) {
            //draw space (size-0,size-1,size-2,...)
            for (int a = 0; a < (size-baris); a++) {
                System.out.print(" ");
            }
            //draw star (1,3,5,..)
            for (int b = 0; b <= (baris*2); b++) {
                if(baris==0||baris==(size-1)|| b==0 || b==(baris*2)){
                    System.out.print("*");
                }else {
                    System.out.print(" ");
                }
            }
            //go to next row (new line)
            System.out.print("\n");
        }
    }
    public static void drawPyramidInverted(int size){
        //for each row
        for(int baris=0;baris<size;baris++) {
            //draw space (0,1,2,...)
            for (int a = 0; a < baris; a++) {
                System.out.print(" ");
            }
            //draw star (7,5,3,..)
            for (int b=1; b<((size-baris)*2); b++) {
                System.out.print("*");
            }
            //go to next row (new line)
            System.out.print("\n");
        }
    }
    public static void drawPyramidInvertedHollow(int size){
        //for each row
        for(int baris=0;baris<size;baris++) {
            //draw space (0,1,2,...)
            for (int a = 0; a < baris; a++) {
                System.out.print(" ");
            }
            //draw star (7,5,3,..)
            for (int b=1; b<((size-baris)*2); b++) {
                if(baris==0||baris==size||b==1||b==(size-baris)*2-1) {
                    System.out.print("*");
                }else{
                    System.out.print(" ");
                }
            }
            //go to next row (new line)
            System.out.print("\n");
        }
    }

}
