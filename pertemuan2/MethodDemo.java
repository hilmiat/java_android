package id.ac.nurulfikri.hilmiat.pertemuan2;

/**
 * Created by my on 8/13/16.
 */
public class MethodDemo {
    //membuat method
    static void greeting(){
        System.out.println("Hello..");
    }
    //method/fungsi yang akan dieksekusi pertama adalah main
    public static void main(String[] args) {
        System.out.println("di dalam method main");
        //memanggil method greeting
        greeting();
        greeting();
        greeting("Adi");
        sapa("Joni");//memanggil method dengan argument
        sapa("Udin");
        int lima = getAngkaLima();
        System.out.println(lima);
        System.out.println(getAngkaLima());
        System.out.println("kuadrt 10:"+kuadrat(10));
        System.out.println("kuadrt 5:"+kuadrat(getAngkaLima()));
    }
    //method yang membutuhkan data (argument)
    static void sapa(String nama){
        System.out.println("Apa kabar "+nama);
    }
    //method dengan return value (memberikan data)
    static int getAngkaLima(){
        int a  = 5;
        return a;
    }
    //method yg membutuhkan dan memberikan data
    static int kuadrat(int angka){
        return angka * angka;
    }
    //method dengan return string
    static String getGreet(){
        return "hello";
    }
    //overloading method
    static void greeting(String nama){
        System.out.println(getGreet());
        sapa(nama);
    }

}
