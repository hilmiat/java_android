package id.ac.nurulfikri.hilmiat.pertemuan2;

import java.util.Scanner;

/**
 * Created by my on 8/13/16.
 */
public class HitungLuas {
    static Scanner s; //dekarasi variabel global s
    public static void main(String[] args) {
        System.out.println("Selamat datang di aplikasi hitung luas");
        boolean mengulang = true;
        while(mengulang) {
            System.out.println("=== Menu ===");
            System.out.println("1. Luas Segitiga\n" +
                                "2. Luas lingkaran\n" +
                                "3. Keluar");
            s = new Scanner(System.in);//inisialisasi scanner
            System.out.print("pilihan anda[1-3]:");
            int pilihan = s.nextInt();
            System.out.println("anda memilih " + pilihan);
            //tentukan arah program berdasarkan pilihan user
            if (pilihan == 1) {
                hitungLuasSegitiga();
            } else if (pilihan == 2) {
                //hitungLuasLingkaran();
                System.out.println("hitung lingkaran");
            } else if (pilihan == 3) {
               mengulang = false;
            }else{
                System.out.println("Pilihan anda salah....");
            }
        }
    }

    private static void hitungLuasSegitiga() {
        //baca alas dan tinggi
        System.out.print("Masukkan alas:");
        int alas = s.nextInt();
        System.out.print("Masukkan tinggi:");
        int tinggi = s.nextInt();
        float luas = hitungLuas(alas,tinggi);
        System.out.println("Luas segitiga dgn alas "+alas+
                " dan tinggi "+ tinggi+" adalah "+luas);
    }

    private static float hitungLuas(int alas, int tinggi) {
        return (float)alas*tinggi/2;
    }
}
