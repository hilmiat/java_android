package id.ac.nurulfikri.hilmiat.pertemuan2;

/**
 * Created by my on 8/13/16.
 */
public class LoopDemo {
    public static void main(String[] args) {
        int i = 0;
        while(i<10){
            i++;
            if(i==3){
                continue;
            }
            System.out.println("Nilai i "+i);
            if (i==8){
                break;
            }
        }

        for(int x=0;x<10;x++){
            if(x==3){
                continue;
            }
            System.out.println("Nilai x:"+x);
        }


    }
}
