package id.ac.nurulfikri.hilmiat.pertemuan4;

/**
 * Created by my on 8/27/16.
 */
public interface KonverterSuhu {
    String satuanAwal();
    String satuanTujuan();
    void setSuhu(float suhu_asal);
    float konversi();
}
