package id.ac.nurulfikri.hilmiat.pertemuan4;

/**
 * Created by my on 8/27/16.
 */
abstract public class Bentuk2D {
    //property / variable (punya apa?)
    private float luas,keliling;
    //method / function (bisa apa?)
    void cetakKeterangan(){
        System.out.println("Bentuk 2D");
    }

    abstract void hitungLuas();

    public float getLuas() {
        return luas;
    }

    public float getKeliling() {
        return keliling;
    }

    public void setLuas(float luas) {
        this.luas = luas;
    }
}
