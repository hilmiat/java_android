package id.ac.nurulfikri.hilmiat.pertemuan4;

/**
 * Created by my on 8/27/16.
 */
public class Ayam extends BinatangDarat{
    public Ayam() {
        //method konstruktor
        //method ini otomatis dipanggil ketika
        //membuat objek ayam
        //new Ayam()
        super.nama="Ayam";
        super.suara="KOKOKOK...";
        super.jml_kaki=2;
    }

}
