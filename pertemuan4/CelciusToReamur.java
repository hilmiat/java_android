package id.ac.nurulfikri.hilmiat.pertemuan4;

/**
 * Created by my on 8/27/16.
 */
public class CelciusToReamur implements KonverterSuhu {
    float suhu_asal;
    @Override
    public String satuanAwal() {
        return "Celcius";
    }
    @Override
    public String satuanTujuan() {
        return "Reamur";
    }
    @Override
    public void setSuhu(float suhu_asal) {
        this.suhu_asal = suhu_asal;
    }
    @Override
    public float konversi() {
        return 4f/5*suhu_asal;
    }
}
