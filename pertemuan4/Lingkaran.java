package id.ac.nurulfikri.hilmiat.pertemuan4;

/**
 * Created by my on 8/27/16.
 */
public class Lingkaran extends Bentuk2D{
    private float jari2;

    public Lingkaran(float jari2) {
        setJari2(jari2);
        hitungLuas();
    }

    @Override
    void hitungLuas() {
        super.setLuas((float) (Math.PI*jari2*jari2));
    }
    public void setJari2(float jari2) {
        this.jari2 = jari2;
    }
}
