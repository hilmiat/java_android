package id.ac.nurulfikri.hilmiat.pertemuan4;

/**
 * Created by my on 8/27/16.
 */
public class DemoAbstract {
    public static void main(String[] args) {
        Bentuk2D lingkaran1 = new Lingkaran(3.5f);
        Bentuk2D segitiga1 = new Segitiga();
//        polymorphisme
        Bentuk2D bentuk2[] = {lingkaran1,segitiga1};

        //kita tdk bisa membuat objek/instance dari kelas abstract
        //Bentuk2D bentuk = new Bentuk2D();

        //kelas anonim didalam kelas lain
        Bentuk2D be = new Bentuk2D() {
            float panjang,lebar;
            @Override
            void hitungLuas() {
                super.setLuas(panjang*lebar);
            }
        };

        System.out.println("luas lingkaran:"+lingkaran1.getLuas());
    }
}
