package id.ac.nurulfikri.hilmiat.pertemuan4;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Created by my on 8/27/16.
 */
public class DemoKonverter {
    public static void main(String[] args) {
        int pilihan;
        Scanner scan;
        while(true) {
            System.out.println("==== Menu ====");
            System.out.println("1. C to F");
            System.out.println("2. C to R");
            System.out.println("3. F to C");
            System.out.println("4. R to C");
            System.out.println("0. Exit");
            scan = new Scanner(System.in);
            System.out.println("pilih 1-4:");
            try {
                pilihan = scan.nextInt();
                System.out.println("pil:" + pilihan);
            } catch (InputMismatchException eee) {
                System.out.println("Pilihan anda tidak tepat");
                continue;
            }
            if(pilihan==0)break;
            KonverterSuhu[] konverter = new KonverterSuhu[5];
            konverter[1] = new CelciusToFahrenheit();
            konverter[2] = new CelciusToReamur();
            try {
                System.out.println("Masukkan suhu " + konverter[pilihan].satuanAwal());
            } catch (Exception e) {
                System.out.println("Pilih 1-4:");
                continue;
            }
            float suhu;
            while(true) {
                scan = new Scanner(System.in);
                try {
                    suhu = scan.nextFloat();
                    break;
                } catch (Exception e) {
                    System.out.println("Masukkan suhu dengan benar...");
                }
            }
            konverter[pilihan].setSuhu(suhu);
            System.out.println("Suhu " + suhu + " " + konverter[pilihan].satuanAwal() + " = " +
                    konverter[pilihan].konversi() + " " + konverter[pilihan].satuanTujuan());

        }
    }
}
