package id.ac.nurulfikri.hilmiat.pertemuan4;

/**
 * Created by my on 8/27/16.
 */
public class CelciusToFahrenheit implements KonverterSuhu {
    float suhu_asal;
    @Override
    public String satuanAwal() {
        return "Celcius";
    }
    @Override
    public String satuanTujuan() {
        return "Fahrenheit";
    }

    @Override
    public void setSuhu(float suhu) {
        suhu_asal = suhu;
    }
    @Override
    public float konversi() {
        return 9f/5*suhu_asal+32;
    }
}
