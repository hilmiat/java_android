package id.ac.nurulfikri.hilmiat.pertemuan4;

/**
 * Created by my on 8/27/16.
 */
public class DemoBinatang {
    public static void main(String[] args) {
        //membuat obj ayam dgn nama a1
//        Ayam a1 = new Ayam();
//        BinatangDarat a1 = new Ayam();

//        System.out.println("Binatang "+a1.nama);
//        a1.berjalan();
//        Kucing k1 = new Kucing();
//        System.out.println("Binatang "+k1.nama);
//        k1.berjalan();

        BinatangDarat[] kebun_binatang = {new Ayam(),new Kucing(),new Ayam()};
        System.out.println("Binatang ke-0:"+kebun_binatang[0].nama);

        for(int i=0;i<kebun_binatang.length;i++){
            System.out.println("Binatang "+kebun_binatang[i].nama);
            kebun_binatang[i].berjalan();
        }

//        for (BinatangDarat b:kebun_binatang){
//            System.out.println("Binatang "+b.nama);
//            b.berjalan();
//        }
    }
}
