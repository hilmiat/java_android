package id.ac.nurulfikri.hilmiat.pertemuan1;
/**
 * Created by my on 8/13/16.
 */
public class VariableDemo {
    public static void main(String ar[]){
        int a; //deklarasi variable
        a = 5; //inisialisasi variable
        System.out.println(a);
        a = 6;
        System.out.println(a);
        int b = 3; //deklarasi + inisialisasi
        System.out.println((float)b/a);
        String greeting = "Hello world";

        double c = a;
        System.out.println(c);
        c = 4.5;
        // b = c; <-- lost precission
        b = (int)c; //<-- dipaksakan
        System.out.println(b);
    }
}
