package id.ac.nurulfikri.hilmiat.pertemuan1;

/**
 * Created by my on 8/13/16.
 */
public class ArrayDemo {
    public static void main(String[] args) {
        int nilai2[] = new int[10]; // definisikan nilai 10 buah
        //isi nialai
        nilai2[0] = 90;
        nilai2[9] = 85;
        //nilai2[99] = 99; <-- indeks melebihi batas

        //deklarasi + inisialisasi array
        float skor[] = {7.5f,4.3f,1.0f};
    }
}
